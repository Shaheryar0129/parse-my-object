_This library has following methods to parse/manipulate your data_

### 1. removeFromObject: (obj: any, keys?: any[]) => any[]

This method remove keys from object or array of object without changing the original one.
1st argument: object or array of object from which you want to remove keys
2nd argument: array of keys you want to remove from your object
example:
```
const obj = [{id:1,name:"Mike"},{id:2,name:"John"},{id:3,name:"Khan"}]
removeFromObject(obj, ['id']) //key id will be deleted

```
### 2. retainFromObject: (obj: any, keys?: any[]) => any[]

This method retain keys that you specify from object or array of object and deletes rest of them without changing the original one.
1st argument: object or array of object from which you want to remove keys
2nd argument: array of keys you want to remove from your object
example:
`retainFromObject(obj, ['id']) //key other than id will be deleted`

### 3. makeStringify: (obj: any, keys?: any[]) => any[]

This method convert specified keys to string
example:
`makeStringify(obj, ['id']) // object key with name id will be converted to string`

### 4. getUniqueArray: (array: any, key: any) => any[]

This methods removes duplicate value of a specified key from an array of object(json)
example:

```
const jsonObj = [
{id:1,name:"Shawn"},
{id:2,name:"Mike"},
{id:1,name:"John"}
]
const result = getUniqueArray(jsonObj,"id")<br>
// result = [id:1,name:"Shawn"},{id:2,name:"Mike"}]

```
### 5.shuffleArray: (array: any) => any

Shuffle the array
example:
const arr = objectFilter.shuffleArray([1, 2, 3, 5, 6]) //result->2,3,6,5,1,4

#### DATE FUNCTIONS

### 6.dateCompCurrent: (date: any, compOpt?: string) => boolean

Compare specified date with current data and returns true or false
1st argument: your specified date
2nd argument: list of options

- gte -> greater than equal to current time
- lte -> less than equal to current time
- gt -> greater than current time
- lt -> less than current time

### 7.dateComp: (date1: any, date2: any, compOpt?: string) => boolean

compare two specified date

1st argument: 1st specified date
2nd argument: 2nd specified date
3rd argument: list of options

- gte -> 2nd date greater than equal to 1st date
- lte -> 2nd date less than equal to 1st date
- gt -> 2nd date greater than 1st date
- lt -> 2nd date less than 1st date

### 8.getDay: (date?: string) => "sunday" | "monday" | "tuesday" | "wednesday" | "thursday" | "friday" | "saturday"

inputs date and returns day

### 9.getCurrentMonth: (date?: string) => "Jan" | "Feb" | "Mar" | "Apr" | "May" | "Jun" | "Jul" | "Aug" | "Sep" | "Oct" | "Nov" | "Dec"

inputs month and returns month

You can fork my project using:
`git clone https://Shaheryar0129@bitbucket.org/Shaheryar0129/parse-my-object.git`
