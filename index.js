const i18n = require("i18n")
const moment = require("moment")

exports.excludeOthers = (obj, keys = []) => {
    if (Array.isArray(obj)) {
        return Object.assign([], obj).map((item) => {
            console.log("\n\nITEM", item)
            for (const key in item) {
                console.log("KEY", key)
                let result = keys.some((k) => {
                    k == key
                })
                console.log("RESULT", result)
                if (!result) {
                    console.log("deleting item.key:", item.key)
                    delete item.key
                }
            }
            console.log("\ndleted ITEM", item)
            return item
        });
    } else {
        return [Object.assign({}, obj)].map((item) => {
            keys.forEach((key) => {
                if (item.hasOwnProperty(key)) {
                } else {
                    delete item[key];
                }
            });
            return item;
        });
    }
}

exports.removeFromObject = (obj, keys = []) => {
    if (Array.isArray(obj)) {
        return Object.assign([], obj).map((item) => {
            keys.forEach((key) => {
                if (item.hasOwnProperty(key)) {
                    delete item[key];
                }
            });
            return item;
        });
    } else {
        return [Object.assign({}, obj)].map((item) => {
            keys.forEach((key) => {
                if (item.hasOwnProperty(key)) {
                    delete item[key];
                }
            });
            return item;
        });
    }
}


exports.retainFromObject = (obj, keys = []) => {
    if (Array.isArray(obj)) {
        return Object.assign([], obj).map((item) => {
            let resObj = {};
            keys.forEach((key) => {
                if (item.hasOwnProperty(key)) {
                    resObj[key] = item[key];
                }
            });
            return resObj;
        });
    } else {
        return [Object.assign({}, obj)].map((item) => {
            let resObj = {};
            keys.forEach((key) => {
                if (item.hasOwnProperty(key)) {
                    resObj[key] = item[key];
                }
            });
            return resObj;
        });
    }
}

exports.makeStringify = (obj, keys = []) => {
    keys.forEach((key) => {
        if (obj.hasOwnProperty(key)) {
            obj[key] = JSON.stringify(obj[key])
        }
    })
    return obj
}

exports.getUniqueArray = (array, key) => {
    const result = [];
    const map = new Map();
    for (const item of array) {
        if (!map.has(item[key])) {
            map.set(item[key], true);    // set any value to Map
            result.push(item);
        }
    }
    return result;
}

exports.uniqueArray = (value, index, self) => {
    return self.indexOf(value) === index;
}

exports.shuffleArray = (array) => {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}


exports.dateCompCurrent = (date, compOpt = 'gte') => {
    let currentTime = new Date();
    let paramDate = new Date(date);
    switch (compOpt) {
        case 'gte':
            return paramDate.getTime() >= currentTime.getTime()
        case 'lte':
            return paramDate.getTime() <= currentTime.getTime()
        case 'gt':
            return paramDate.getTime() > currentTime.getTime()
        case 'lt':
            return paramDate.getTime() < currentTime.getTime()
        default:
            return paramDate.getTime() >= currentTime.getTime()
    }
}


exports.dateComp = (date1, date2, compOpt = 'gte') => {
    let currentTime = new Date(date1);
    let paramDate = new Date(date2);
    switch (compOpt) {
        case 'gte':
            return paramDate.getTime() >= currentTime.getTime()
        case 'lte':
            return paramDate.getTime() <= currentTime.getTime()
        case 'gt':
            return paramDate.getTime() > currentTime.getTime()
        case 'lt':
            return paramDate.getTime() < currentTime.getTime()
        default:
            return paramDate.getTime() >= currentTime.getTime()
    }
}

exports.getDay = (date = Date()) => {
    let paramDate = new Date(date);
    switch (paramDate.getDay()) {
        case 0:
            return "sunday";
        case 1:
            return "monday";
        case 2:
            return "tuesday";
        case 3:
            return "wednesday";
        case 4:
            return "thursday";
        case 5:
            return "friday";
        case 6:
            return "saturday";
        default:
            return;
    }
}

exports.getCurrentMonth = (date = Date()) => {
    let paramDate = new Date(date);
    switch (paramDate.getMonth()) {
        case 0:
            return "Jan";
        case 1:
            return "Feb";
        case 2:
            return "Mar";
        case 3:
            return "Apr";
        case 4:
            return "May";
        case 5:
            return "Jun";
        case 6:
            return "Jul";
        case 7:
            return "Aug";
        case 8:
            return "Sep";
        case 9:
            return "Oct";
        case 10:
            return "Nov";
        case 11:
            return "Dec";
        default:
            return;
    }
}

exports.getInterval = (range = "week", lastPart = 1) => {
    let currentTime = moment(),
        date = [],
        i = lastPart;

    for (let j = 0; j < i; j++) {
        let dates = {
            from: moment(currentTime).startOf(range),
            to: moment(currentTime).endOf(range)
        }
        currentTime = currentTime.subtract(range, 1);
        date.unshift(dates)
    }
    return date;
}

exports.getTimePeriod = (range = 'week', lastPart = 1) => {
    return {
        from: moment().subtract(lastPart, range).startOf('day'),
        to: moment().endOf('day')
    }
}